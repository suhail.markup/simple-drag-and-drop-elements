import React, { Fragment } from "react";
import "./App.scss";
import Header from "./pages/common-components/header/Header";
import WorkSpace from "./pages/workspace/WorkSpace";

const App = () => {
  return (
    <Fragment>
      <Header />

      <div className="content-wrap">
        <WorkSpace />
      </div>
    </Fragment>
  );
};

export default App;
