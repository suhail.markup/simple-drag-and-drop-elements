import React from "react";
import { Draggable, Droppable } from 'react-drag-and-drop';
import InputBox from "./Components/InputBox";
import SelectBox from "./Components/SelectBox";
import CheckBox from "./Components/CheckBox";
import RadioButton from "./Components/RadioButton";


const WorkSpace = () => {
  return (
    <div className="workspace">
      <aside>
        <h2>Left Panel</h2>

        {/* input box starts */}
        <Draggable>
          <InputBox />
        </Draggable>
        {/* input box ends */}

        {/* select box starts */}
        <Draggable>
          <SelectBox />
        </Draggable>
        {/* select box ends */}

        {/* checkbox starts */}
        <Draggable>
          <CheckBox />
        </Draggable>
        {/* checkbox ends */}

        {/* radio button starts */}
        <Draggable>
          <RadioButton />
        </Draggable>
        {/* radio button ends */}

        {/* button starts */}
        <Draggable>
          <button class="btn btn-primary">Simple Button</button>
        </Draggable>
        {/* button ends */}
      </aside>

      <div className="workspace-container">
        <div className="left-col">
          <h2>WorkSpace</h2>

          <Droppable className="droppable-content-wrapper" />
        </div>
        <div className="right-col">
          <h2>Json Structure</h2>
        </div>
      </div>
    </div>

  );
};

export default WorkSpace;
