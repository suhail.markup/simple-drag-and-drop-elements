import React from "react";


const InputBox = () => {
  return (
    <div className="form-group mb-32">
      <label>Input field</label>
      <input type="text" className="form-control" placeholder="Text field" />
    </div>
  );
};

export default InputBox;
