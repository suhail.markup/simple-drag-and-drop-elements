import React from "react";


const SelectBox = () => {
  return (
    <div className="form-group mb-32">
      <label>Select field</label>
      <div className="select-wrapper">
        <select className="form-control" id="exampleSelect1">
          <option value disabled>Lorem ipsum</option>
          <option value>Lorem ipsum</option>
          <option value>Lorem ipsum</option>
          <option value>Lorem ipsum</option>
          <option value>Lorem ipsum</option>
        </select>
      </div>
    </div>
  );
};

export default SelectBox;
