import React from "react";


const RadioButton = () => {
  return (
    <div className="form-group">
      <label>Radio button</label>
      <div className="vertical-center">
        <div className="custom-radiobtn">
          <input name="option1" type="radio" id="opt1_1" defaultValue="Option" defaultChecked />
          <label htmlFor="opt1_1" id>
            Radio 1
        <i />
          </label>
        </div>
        <div className="custom-radiobtn">
          <input name="option1" type="radio" id="opt1_2" defaultValue="Option" />
          <label htmlFor="opt1_2" id>
            Radio 1
        <i />
          </label>
        </div>
      </div>
    </div>
  );
};

export default RadioButton;
