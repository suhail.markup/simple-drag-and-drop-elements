import React from "react";


const CheckBox = () => {
  return (
    <div className="form-group mb-32">
      <label>Checkbox</label>
      <div className="custom-formcheck">
        <input type="checkbox" id="opt__1" className="form-check-input" defaultChecked />
        <label className="form-check-label" htmlFor="opt__1">
          Check Box
              <i className="checkbox-icon" />
        </label>
      </div>
    </div>
  );
};

export default CheckBox;
