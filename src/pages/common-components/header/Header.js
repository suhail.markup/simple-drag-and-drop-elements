import React from "react";

const Header = () => {
  return (
    <header>
      <div className="left-col">
        <a href="#">
          <img src="https://i.imgur.com/YeyMzvR.png" />
        </a>
      </div>

      <div className="right-col">
        <button className="btn btn-primary">Publish</button>
      </div>
    </header>
  );
};

export default Header;
